package ooss;

import java.util.*;

public class Teacher extends Person implements Notification{
    private List<Klass> klasses = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce(){
        StringJoiner klassesList = new StringJoiner(", ");
        for (Klass klass : klasses) {
            klassesList.add(String.valueOf(klass.getNumber()));
        }
        return super.introduce() + " I am a teacher." + (klassesList.length() > 0 ? String.format(" I teach Class %s.",klassesList.toString()) : "");
    }

    public void assignTo(Klass klass){
        klasses.add(klass);
        klass.addObserver(this);
    }

    public boolean belongsTo(Klass klass){
        return klasses.contains(klass);
    }

    public boolean isTeaching(Student student){
        return klasses.contains(student.getKlass());
    }

    @Override
    public void notify(int classNumber, String classLeader) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.",this.getName(), classNumber,classLeader);
    }
}
