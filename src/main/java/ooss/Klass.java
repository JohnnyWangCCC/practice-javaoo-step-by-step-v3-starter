package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    private int number;

    private Student klassLeader;

    private List<Person> observerListeners = new ArrayList<>();

    public void addObserver(Person person) {
        observerListeners.add(person);
    }

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Klass){
            return this.number == ((Klass) obj).number;
        }
        return super.equals(obj);
    }

    public void assignLeader(Student king) {
        if (this.equals(king.getKlass())){
            this.klassLeader = king;
            System.out.printf("I am the leader of class %d.%n", this.number);
        }else {
            System.out.println("It is not one of us.");
        }

        notifyObservers();
//        if (teacher != null){
//            System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.",teacher.getName(),this.getNumber(),this.klassLeader.getName());
//        }
//        students.forEach((item -> System.out.printf("I am %s, student of Class %d. I know %s become Leader.",item.getName(),this.getNumber(),this.klassLeader.getName())));

    }

    private void notifyObservers() {
        for (Person observerListener : this.observerListeners) {
            ((Notification) observerListener).notify(this.number,this.klassLeader.getName());
        }
    }

    public boolean isLeader(Student tom) {
        return klassLeader.equals(tom);
    }

    public void attach(Person person) {
        if (person == null || this.klassLeader == null){
            return;
        }
        System.out.println(String.format("I am %s, teacher of Class 2. I know %s become Leader.",person.getName(), this.klassLeader.getName()));
    }

    public Student getKlassLeader() {
        return klassLeader;
    }
}
