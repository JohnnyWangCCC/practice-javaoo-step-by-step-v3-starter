package ooss;

public interface Notification {
    public void notify(int classNumber, String classLeader);
}
