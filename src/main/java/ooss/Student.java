package ooss;

public class Student extends Person implements Notification {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce(){
        if (klass != null && this.equals(klass.getKlassLeader())){
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.",this.getName(),this.getAge(),klass.getNumber());
        }
        return super.introduce() + " I am a student." + (this.klass != null ? String.format(" I am in class %d.",this.klass.getNumber()) : "");
    }

    public void join(Klass klass){
        this.klass = klass;
        klass.addObserver(this);
    }

    public boolean isIn(Klass klass){
        if (klass == null || this.klass == null){
            return false;
        }
        return klass.getNumber() == this.klass.getNumber();
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

    @Override
    public void notify(int classNumber, String classLeader) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.",this.getName(), classNumber,classLeader);
    }
}
